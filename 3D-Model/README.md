## Folder `3D-Model`
This folder contains all the 3D models that we used to fabricate our experimental apparatus for future replication of our experiment.

- Folder `rigid stimuli`
This folder contains a .scad code file that can be opened with the OpenSCAD software to generate the 3D models of the rigid stimuli.

- Folder `soft stimuli`
This folder contains OpenSCAD code for generating all 3D models used for fabricating our soft stimuli. First, one creates 3D models with different curvatures by modifying the variable `R` in each OpenSCAD code file. Second, the software OpenSCAD allows to generate STL files of these 3D models. Third, STL files can then be printed with a 3D printer. Finally, once printed, one can insert the printed bases (models in the subfolder `base`) into the printed molds (models in the subfolder `mold`) filled with uncured silicone to fabricate the soft stimuli.

  - Folder `mold`
This folder contains a SCAD code file for generating 3D models of molds for soft stimuli. This code can be opened with OpenSCAD.

  - Folder `base`
This folder contains a SCAD code file for generating 3D models of bases for soft stimuli. This code can be opened with OpenSCAD.

- Folder `support`
This folder contains all the 3D models that we used for fabricating the support of our stimuli and the slide where the stimuli enter and exit during the experiment (Figure 2(a) in the paper). They are in format STL. You can print them directly with a 3D printer.

- Folder `box`
This folder contains SVG files of the wood box that we used in our experiment (Figure 2(a) in the paper). You can fabricate it with a laser cutter.
