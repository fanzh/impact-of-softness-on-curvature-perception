R=52.6;//Radius of curved surface
r_w=8;//radius of the base cylinder
h=R-sqrt(R*R-r_w*r_w);
echo(h);
H_total=16;//Total height

intersection(){
        translate([0,0,h/2])cube([r_w*2,r_w*2,h],center=true);
        translate([0,0,h-R])sphere(R,$fn=1000);
    }
h_base=H_total-h;
translate([0,0,-h_base])cylinder(r=r_w,h=h_base,$fn=100);

 module dowel(h,L,w){
        //cube([w,L,h],center=true);
        linear_extrude(height = h, center = true, convexity = 10, slices = 20, scale = 1.1, $fn = 16)
        square([L,w],center=true);
        rotate([0,0,90])
        linear_extrude(height = h, center = true, convexity = 10, slices = 20, scale = 1.1, $fn = 16)
        square([L,w],center=true);
     //cube([w,L,h],center=true);
 }
 
h_dowel=5;
 translate([0,0,-h_base+h_dowel/2])dowel(5,r_w*2+10,5);
    

     
