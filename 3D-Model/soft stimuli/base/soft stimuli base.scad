R=52.6;//Radius of curved surface
r_w=8;//radius of the base cylinder
h=R-sqrt(R*R-r_w*r_w);
H_total=16;//Total height


h_base=H_total-h;
    h_dowel=5;
    
h_sup=3;
difference(){
    union(){
    translate([0,0,-h_base])cylinder(r1=r_w,r2=r_w,h=h_base-h_sup,$fn=100);
    translate([0,0,-h_base+h_dowel/2])dowel(5,r_w*2+10,5,R);
    
    }
    //old thickness 1.75mm new thickness 1
    union(){
        translate([0,0,-h_base-0.9])cylinder(r=r_w-1,h=h_base+1,$fn=100);
    }
}

translate([0,0,-h_base+3/2+5-1.1])dowel(3,r_w*2-1.75,3);

difference(){
    difference(){
            translate([0,0,-h_sup-0.1])cylinder(r1=r_w-1,r2=r_w-1,h=h_sup+0.1-1,$fn=100);//with 1 mm gap between resin and silicone
            translate([0,0,-h_sup-0.1])cylinder(r1=r_w-2,r2=r_w-2,h=h_sup+0.1-1,$fn=100);//with 1 mm gap between resin and silicone
        }
    union(){
           translate([0,0,-h_sup]) cube([r_w*2,5,h_sup-1],center=true);
            translate([0,0,-h_sup]) rotate([0,0,90]) cube([r_w*2,5,h_sup-1],center=true);
        }
}

                    

 module dowel(h,L,w,R){
        //cube([w,L,h],center=true);
        difference(){
            union(){
                linear_extrude(height = h, center = true, convexity = 10, slices = 20, scale = 1.1, $fn = 16)
                square([L,w],center=true);
                }
           
        union(){
                    translate([r_w,5,-1])
                rotate([90,0,0])
                            linear_extrude(height=10)
                            text(text=str(R),size=2);

        translate([-r_w-5,5,-1])
                rotate([90,0,0])
                            linear_extrude(height=10)
                    text(text=str(R),size=2);
            }
        }
        
        difference(){
            union(){
            rotate([0,0,90])
            linear_extrude(height = h, center = true, convexity = 10, slices = 20, scale = 1.1, $fn = 16)
            square([L,w],center=true);
            }
            union(){
                    translate([5,r_w+5,-1])
                rotate([90,0,-90])
                            linear_extrude(height=10)
                            text(text=str(R),size=2);

        translate([5,-r_w,-1])
                rotate([90,0,-90])
                            linear_extrude(height=10)
                    text(text=str(R),size=2);
            }
            }
     //cube([w,L,h],center=true);
 }

