r_w=8;//radius of the base cylinder
H_total=16;//Total height
R=52.6;//Radius of curved surface


resolution=100;
sizeModule=35;
 module dowel(h,L,w){
        //cube([w,L,h],center=true);
        linear_extrude(height = h, center = true, convexity = 10, slices = 20, scale = 1.1, $fn = 16)
        square([L,w],center=true);
        rotate([0,0,90])
        linear_extrude(height = h, center = true, convexity = 10, slices = 20, scale = 1.1, $fn = 16)
        square([L,w],center=true);
     //cube([w,L,h],center=true);
        translate([0,L/2,-h/2])cylinder(r=w/2,h=h,$fn=100);
        translate([0,L/2,0])cube([w-4,L,h+1],center=true);
        //translate([0,L/2,,0])cube([w-4,L,h],center=true);
 }

 module createSoftCurvedSurface(curvatureR,nSegments,sizeModule){
     curvature_h=curvatureR-sqrt(curvatureR*curvatureR-r_w*r_w);
     h_base=H_total-curvature_h;
     translate([0,0,h_base]){
        difference(){
            translate([0,0,(H_total+5)/2-h_base])cube([sizeModule,sizeModule,H_total+5],center=true);
            union(){
                    intersection(){
                            translate([0,0,curvature_h/2])cube([r_w*2,r_w*2,curvature_h],center=true);
                            translate([0,0,curvature_h-curvatureR])sphere(curvatureR,$fn=nSegments);
                        }
                    translate([0,0,-h_base])cylinder(r=r_w+0.15,h=h_base,$fn=100);//original r=r_w+0.5
                    h_dowel=5;
                    translate([0,0,-h_base+h_dowel/2])dowel(5,(r_w*2+10)*1.1+1.5,5*1.1+1.5);

                    translate([r_w/2,r_w,-h_base])
                    linear_extrude(height=3)
                    text(text=str(curvatureR),size=3);

            }

        }
    }

}





createSoftCurvedSurface(R,resolution,sizeModule);
     


// Bach commande can help to reduce rendering time /Applications/OpenSCAD.app/Contents/MacOS/OpenSCAD -o ../TryBatchOpenScasdWithVar.stl -D "Size=1" -D 'quality="production"' ../testBatch.scad 
