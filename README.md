# Impact of softness on users’ perception of curvature for future soft curvature-changing UIs

This repository contains materials to replicate our psychophysical experiment published at the CHI23 conference, the data that we collected during this experiment, and code to analyze the data. To cite this project, please cite the paper from https://doi.org/10.1145/3544548.3581179 . You can also find the open-access version at https://hal.science/hal-04045261 . Before we started our experiment and collected data, we registered our experiment by Open Science Framework at https://osf.io/scnj9 .

This repository contains one PDF file `SupplementaryCalculation.pdf`, one mp4 file `demo video of experiment.MP4`, and three folders `dataCollectionSoftware`, `analyzingCodeAndExperimentData`, and `3D-Model`. Below is the description of each folder and file. For further details on each folder, please reach the respective folder.

## File `SupplementaryCalculation.pdf`
This file serves as supplementary material for the paper https://doi.org/10.1145/3544548.3581179. It presents the detail of the calculation of the impact of stimulus width on the total height change of the stimulus, discussed in the discussion section of the paper.

## File `demo video of experiment.MP4`
This video figures out how the stimuli are guided for quick and accurate manual switch between stimuli during our experiment.

## Folder `dataCollectionSoftware` 
This folder contains the software that we developed for collecting experiment data during the experiment. It contains one folder `qtCode` where you can find codes for our major data-collecting software developed in Python with PyQT5, one folder `teensyCode` where you can find the code for a Teensy2 which measured the force that participants applied on our stimuli during the experiment. The teensy send measured data by its serial port to the computer where we run our major data-collecting software. 

## Folder `analyzingCodeAndExperimentData`
This folder contains one folder `data` where you can find our experiment data, our measurement data of softness and curvature of our stimuli and the R codes to analyze the data, and the results from our experiment presented in the paper https://doi.org/10.1145/3544548.3581179 .

## Folder `3D-Model`
This folder contains all the 3D models that we used to fabricate our experimental apparatus for future replication of our experiment.
