## Folder `teensyCode`

### Folder `readForceSensor`
This folder contains the code for a Teensy2 reading the force data from 7 force sensors, i.e., FSR406 from InterlinkElectronics. The 7 sensors includes 1 sensor for the reference stimulus, ans 6 sensors for 6 comparison stimulus in one curvature and softness condition. The teensy receives the commande by the serial port from the computer where we run the major data-collecting software. Depending on the commande received, the Teensy read data from respective force sensor and send it to the computer.

### Folder `data sheet`
This folder contains data steets for electronic components that we used for force measurement.

- File `lm158-n.pdf`
This file is a data sheet for the amplifier that we used.

- File `FSR Integration Guide - Interlink Electronics.pdf` 
This file is a data sheet for the force sensor that we used.
