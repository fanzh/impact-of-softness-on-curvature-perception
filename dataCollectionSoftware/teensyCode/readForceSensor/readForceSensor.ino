/*
 * Created by ArduinoGetStarted.com
 *
 * This example code is in the public domain
 *
 * Tutorial page: https://arduinogetstarted.com/tutorials/arduino-force-sensor
 */

#define FORCE_SENSOR_COMP1 A0 // 
#define FORCE_SENSOR_COMP2 A1 // 
#define FORCE_SENSOR_COMP3 A2 // 
#define FORCE_SENSOR_COMP4 A3 // 
#define FORCE_SENSOR_COMP5 A4 // 
#define FORCE_SENSOR_COMP6 A5 // 
#define FORCE_SENSOR_REF A8 // 

int numPinToRead=0;

int analogReadingComp1 = 0;
int analogReadingComp2 = 0;
int analogReadingComp3 = 0;
int analogReadingComp4 = 0;
int analogReadingComp5 = 0;
int analogReadingComp6 = 0;
int analogReadingRef = 0;

void setup() {
  Serial.begin(9600);
}

//bool statusSendSensorValue= true;
//String readInstruction;
void loop() {

if (Serial.available() > 0) {
    numPinToRead = Serial.readString().toInt();
    Serial.flush();
  }
  switch (numPinToRead){
      case 0 :
       break;
      case 1:
        analogReadingComp1 = analogRead(FORCE_SENSOR_COMP1);
        break;
      case 2:
        analogReadingComp2 = analogRead(FORCE_SENSOR_COMP2);
        break;
      case 3:
        analogReadingComp3 = analogRead(FORCE_SENSOR_COMP3);
        break;
      case 4:
        analogReadingComp4 = analogRead(FORCE_SENSOR_COMP4);
        break;
      case 5:
        analogReadingComp5 = analogRead(FORCE_SENSOR_COMP5);
        break;
      case 6:
        analogReadingComp6 = analogRead(FORCE_SENSOR_COMP6);
        break;
      case 7:
        analogReadingRef = analogRead(FORCE_SENSOR_REF);
        break;
      default:
        break;
    }

    switch (numPinToRead){
      case 0 :
       break;
      case 1:
        Serial.println(analogReadingComp1);
        break;
      case 2:
        Serial.println(analogReadingComp2);
        break;
      case 3:
        Serial.println(analogReadingComp3);
        break;
      case 4:
        Serial.println(analogReadingComp4);
        break;
      case 5:
        Serial.println(analogReadingComp5);
        break;
      case 6:
        Serial.println(analogReadingComp6);
        break;
      case 7:
        Serial.println(analogReadingRef);
        break;
      default:
        break;
    }
    
    delay(80);

}
