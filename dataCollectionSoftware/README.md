## Folder `dataCollectionSoftware` 

### Folder `qtCode`
This folder contains codes for our major data-collecting software developed in Python with PyQT5.


### Folder `teensyCode`
This folder contains the code for a Teensy2 which measured the force that participants applied on our stimuli during the experiment. The teensy send measured data by its serial port to the computer where we run our major data-collecting software. This folder also contains data sheets for electronic components that we used for force measurement.

### Quick start
Our software was designed to collect experiment data from 12 participants. You can adapt it for more or fewer participants.
#### Generating the configuration file which plans the order of stimulus presented to participants in each trial.

- Run the Python script `qtCode/createXpParaXML.py` to create a configuration file, `experimentsPara.XML`, which plans the order of stimulus presented to participants in each trial. This script was defined to generate a configuration file for 12 participants. You can adapt it to your case by modifying the variable `numParticipants` in `qtCode/createXpParaXML.py`. You can also find one XML file named `qtCode/experimentsParaFixed.xml`, which is the configuration file that we used for our experiment.

#### Download the fore measurement code to a Teensy2 board and connect it to the computer via a serial port.
You can find the code for Teensy, `readForceSensor.ino`, from the folder `teensyCode/readForceSensor`. We used 7 force sensors: 6 sensors connected to pins A0-A5 for 6 comparison stimuli in one softness and curvature condition, and 1 sensor connected to pin A7, for the respective reference stimulus. If you do not need to collect force data, you can ignore this step and change L189 in the script `softnessCurvature.py` in the `qtCode` folder from `self.enableSerialPort = True` to `self.enableSerialPort = False`.

#### Generating the configuration file which plans the order of stimulus presented to participants in each trial.

- Run the Python script `createXpParaXML.py` to create a configuration file, `experimentsPara.XML`, which plans the order of stimulus presented to participants in each trial. This script was defined to generate a configuration file for 12 participants. You can adapt it to your case by modifying the variable `numParticipants` in `createXpParaXML.py`. You can also find one XML file named `experimentsParaFixed.xml`, which is the configuration file that we used for our experiment.

#### Doing experiment and collecting experiment data
For each participant, s/he experiment `3 series`(one series for each curvature condition) × `4 blocks` (one block for each softness condition) × `3 sub-blocks` (each block contains 3 sub-blocks as repetitions) × `12 trials` (one trial is to compare a comparison stimulus with respective reference stimulus. In each sub-block, a participant compares twice the 6 comparison stimuli with the reference stimulus, leading to 12 trials in one sub-block). For details of our experiment procedures please see the `Section 3 Experiment` of our paper https://doi.org/10.1145/3544548.3581179 . 

- **Step 1: Start the experiment** Run the Python script `softnessCurvature.py` in the `qtCode` folder and choose with which participant you are doing your experiment.

<img
 src="./qtCode/figures/startPage.png"
 alt="start page"
 title="start page"
 style="display: inline-block; margin: 0 auto; max-width: 300px">
  
  
- **Step2:Connect to Teensy board (optional)** Choose the serial port where you connect your Teensy board, e.g., here `dev/cu.usbmodem1434101` was chosen in our case. If you do not need to collect force data, you can ignore this and click directly the button "Start XP" to start the experiment. In this step, you can also see the order of the experiment series of this participant from the text browser `Curvature Order`. E.g., here, `10,20,40` means this participant will first experiment with the curvature `10 mm` condition, then the `20 mm` condition, and finish with the `40 mm` condition. 

<img
 src="./qtCode/figures/experimentParaPage.png"
 alt="experimentPara Page"
 title="experimentPara Page"
 style="display: inline-block; margin: 0 auto; max-width: 300px">
  
  
- **Step3: Start an experiment series** Prepare stimuli. In this step, you can see the order of experiment blocks of this participant for this experiment series from the text browser `Softness Order`. E.g., here, `00-10,00-50, Rigid, A-30` means, in this experiment series (`20 mm` condition as indicated in the text browser `Ref Curvature`), this participant first experiments with the softness `Shore00-10` condition, then `Shore00-50` condition, then `Rigid` condition and finally `ShoreA-30` condition. The text browser `Training Softness` indicates the softness of stimuli with which participants do the training before this experiment series, e.g., here, `Shore00-50`. With this information, you can prepare stimuli both for the training and also for this experiment block.

<img
 src="./qtCode/figures/seriesStartPage.png"
 alt="seriesStart Page"
 title="seriesStart Page"
 style="display: inline-block; margin: 0 auto; max-width: 300px">
  
- **Step4: Start an experiment block** As indicated in this figure, in this experiment block, all stimuli will be presented in the curvature `10 mm` condition (as indicated in the text browser `Ref Curvature`) and in the softness `Shore00-10` condition (as indicated in the text browser `Softness`). From the text browser `Curvature`, you can see the order of presenting stimuli in the first sub-block for this experiment block. Two stimuli from different trials were separated with a semicolon. Two stimuli in one trial were separated with a comma, first, one is firstly presented and the second one is secondly presented. E.g., For the first two stimuli in the text browser `Curvature` "10.7,10" means in the first trial the experimenter first presents the participant with a stimulus with the curvature of `10.7 mm` and then a stimulus of `10 mm`. 
 
<img
 src="./qtCode/figures/blockStartPage.png"
 alt="blockStart Page"
 title="blockStart Page"
 style="display: inline-block; margin: 0 auto; max-width: 300px">

- **Step 5: Start an experiment trial** The experimenter prepares two stimuli used in this trial. E.g., the first stimulus has a curvature of `10.7 mm` as indicated by the text browser `Curvature1`, and the second stimulus has a curvature of `10 mm` as indicated by the text browser `Curvature2`. Then the experimenter presents the first stimulus to the participant and asks her or him to start the exploration. Once the participant touches and starts exploring the first stimulus， the experimenter clicks the button `Next` to pass to the next step.
 
<img
 src="./qtCode/figures/trialStartPage.png"
 alt="trialStart Page"
 title="trialStart Page"
 style="display: inline-block; margin: 0 auto; max-width: 300px">
  
- **Step 6: Explore the first stimulus** The text browser `Start Time 1` indicates the starting time of exploring the first stimulus. Once the participant signals the experimenter s/he completing exploring the first stimulus, the experimenter clicks the button `Next` to pass to the next step.  
<img
 src="./qtCode/figures/phase1.png"
 alt="phase1 Page"
 title="phase1 Page"
 style="display: inline-block; margin: 0 auto; max-width: 300px">


- **Step 7: Complete exploring the first stimulus** The text browser `End Time 1` indicates the ending time of exploring the first stimulus. Then the experimenter switches the stimuli and presents the participant with the second stimulus. The experimenter asks the participant to start the exploration. Once the participant touches and starts exploring the second stimulus， the experimenter clicks the button `Next` to pass to the next step.
<img
 src="./qtCode/figures/phase2.png"
 alt="phase2 Page"
 title="phase2 Page"
 style="display: inline-block; margin: 0 auto; max-width: 300px">
  
- **Step 8: Explore the second stimulus** The text browser `Start Time 2` indicates the ending time of exploring the second stimulus. Once the participant signals the experimenter s/he completing exploring the second stimulus, the experimenter clicks the button `Next` to pass to the next step. At the same time, the participant told the experimenter which stimulus is curvier between the two stimuli presented by answering "first" or "second".
<img
 src="./qtCode/figures/phase3.png"
 alt="phase3 Page"
 title="phase3 Page"
 style="display: inline-block; margin: 0 auto; max-width: 300px">
  
- **Step 9: Explore the second stimulus** The text browser `End Time 2` indicates the starting time of exploring the second stimulus. The experimenter clicks the button `Next` to pass to the next step.  
<img
 src="./qtCode/figures/phase4.png"
 alt="phase4 Page"
 title="phase4 Page"
 style="display: inline-block; margin: 0 auto; max-width: 300px">
  
- **Step10: Record participant's answer** The participant clicks the radio button `First` or `Second` to record the participant's answer for this trial. Then, click the button `Next Trial` to start the next trial.
<img
 src="./qtCode/figures/phase5.png"
 alt="phase5 Page"
 title="phase5 Page"
 style="display: inline-block; margin: 0 auto; max-width: 300px">
  
  
- **Analyze experiment data** Copy CSV data under the folder `qtCode/data/` to `../analyzingCodeAndExperimentData/experimentData/` and run respective R codes to analyze the data. 

### Notices
-  **Breaks during the experiment** when breaks occur during the experiment, whether intentionally or unintentionally, you can note in which experiment series, experiment block, experiment sub-block, and experiment trials the break occurs. Then modify respect variables `self.noSeries`, `self.noBlock`, `self.noSubBlock`, and `self.noTrial` in the script `qtCode/softnessCurvature.py` to it. One typical intentional break is that when a participant finishes one series, s/he does another series on another half day. One typical unintentional break is when the serial port cable connecting the Teensy board and computer is disconnected unintentionally. 

-  **Furthur improvement** Participants' exploring force data during the exploration are currently saved in folders, e.g., folder `qtCode/data/Participant9Force/` contain the exploring force data of participant9. One further improvement is to display the exploring force data in real-time in our experiment data-collecting software GUI. 
