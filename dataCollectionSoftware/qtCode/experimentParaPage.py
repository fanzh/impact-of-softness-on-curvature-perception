# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file './experimentParaPage.ui'
#
# Created by: PyQt5 UI code generator 5.15.6
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_experimentParaPage(object):
    def setupUi(self, experimentParaPage):
        experimentParaPage.setObjectName("experimentParaPage")
        experimentParaPage.resize(1200, 800)
        self.startXpPushButton = QtWidgets.QPushButton(experimentParaPage)
        self.startXpPushButton.setGeometry(QtCore.QRect(20, 660, 1121, 71))
        font = QtGui.QFont()
        font.setPointSize(40)
        self.startXpPushButton.setFont(font)
        self.startXpPushButton.setAutoFillBackground(False)
        self.startXpPushButton.setStyleSheet("background-color: rgb(10, 176, 176);")
        self.startXpPushButton.setObjectName("startXpPushButton")
        self.label_3 = QtWidgets.QLabel(experimentParaPage)
        self.label_3.setGeometry(QtCore.QRect(10, 170, 301, 48))
        font = QtGui.QFont()
        font.setPointSize(40)
        self.label_3.setFont(font)
        self.label_3.setObjectName("label_3")
        self.curvatureOrderTextBrowser = QtWidgets.QTextBrowser(experimentParaPage)
        self.curvatureOrderTextBrowser.setGeometry(QtCore.QRect(310, 170, 806, 131))
        font = QtGui.QFont()
        font.setPointSize(40)
        self.curvatureOrderTextBrowser.setFont(font)
        self.curvatureOrderTextBrowser.setObjectName("curvatureOrderTextBrowser")
        self.participantIdTextBrowser = QtWidgets.QTextBrowser(experimentParaPage)
        self.participantIdTextBrowser.setGeometry(QtCore.QRect(307, 10, 806, 131))
        font = QtGui.QFont()
        font.setPointSize(40)
        self.participantIdTextBrowser.setFont(font)
        self.participantIdTextBrowser.setObjectName("participantIdTextBrowser")
        self.label = QtWidgets.QLabel(experimentParaPage)
        self.label.setGeometry(QtCore.QRect(50, 10, 249, 48))
        font = QtGui.QFont()
        font.setPointSize(40)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.label_6 = QtWidgets.QLabel(experimentParaPage)
        self.label_6.setGeometry(QtCore.QRect(320, 360, 391, 48))
        font = QtGui.QFont()
        font.setPointSize(40)
        self.label_6.setFont(font)
        self.label_6.setObjectName("label_6")
        self.forceSensorSerialPortcomboBox = QtWidgets.QComboBox(experimentParaPage)
        self.forceSensorSerialPortcomboBox.setGeometry(QtCore.QRect(280, 440, 471, 81))
        self.forceSensorSerialPortcomboBox.setObjectName("forceSensorSerialPortcomboBox")

        self.retranslateUi(experimentParaPage)
        QtCore.QMetaObject.connectSlotsByName(experimentParaPage)

    def retranslateUi(self, experimentParaPage):
        _translate = QtCore.QCoreApplication.translate
        experimentParaPage.setWindowTitle(_translate("experimentParaPage", "Form"))
        self.startXpPushButton.setText(_translate("experimentParaPage", "Start XP"))
        self.label_3.setText(_translate("experimentParaPage", "Curvature Order :"))
        self.curvatureOrderTextBrowser.setHtml(_translate("experimentParaPage", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'.AppleSystemUIFont\'; font-size:40pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body></html>"))
        self.participantIdTextBrowser.setHtml(_translate("experimentParaPage", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'.AppleSystemUIFont\'; font-size:40pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body></html>"))
        self.label.setText(_translate("experimentParaPage", "Participant ID :"))
        self.label_6.setText(_translate("experimentParaPage", "ForceSensor serial Port"))
