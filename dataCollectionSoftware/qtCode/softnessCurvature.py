#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec  1 15:48:20 2021

@author: fanzhuzhi
"""
import traceback,sys
import time
import serial
from serial.tools import list_ports 
import csv
import math

from PyQt5 import QtWidgets
from PyQt5.QtWidgets import *
from xml.etree import ElementTree, cElementTree
from xml.etree.ElementTree import Element,SubElement,Comment,tostring
from PyQt5.QtCore import *
# import serial

from startPage import Ui_startPage
from experimentParaPage import Ui_experimentParaPage
from seriesStartPage import  Ui_seriesStartPage
from blockStartPage import Ui_blockStartPage
from trialStartPage import Ui_trialStartPage
from trialResultPage import Ui_trialResultPage
from finishedPage import Ui_finishedPage

from random import randint

"""
State machine:
    State start
    State xpPara
    State blockStart
    State trialStart 
        State trialPhase1
        State trialPhase2
        State trialPhase3
        State trialPhase4
    State trialResult
    
    Init:
        noSeries=0
        numSeries=4
        noBlock=0
        numBlock=3
        noTrials=0
        numTrials=12
        noPhase=0
        numPhase=4
    
    State start   (participant ID entered & Start button clicked)       State xpPara
                  ----------------------------------------------------->
                  load participant ID, softness order
                  
                  
    State xpPara   (Start XP button clicked)                            State seriesStart     
                    --------------------------------------------------->  
                    load noSeries/numSeries, curvature ref order, softness in this series
                    
                    
    State seriesStart   (Start Series button clicked)    State blockStart      
                    ------------------------------------------------------------> 
                        load noBlock/numBlocks, softness in this series, curvature order in this block
                        
                                            
    State blockStart  (Start Trials button clicked)   State trialStart
                    ------------------------------------------------------->
                    load noTrials/numTrials,load noTrials/numTrials, curvature1, curvature2,softness
                            
                    
    State trialStart   (Next button clicked)                              State trialPhase1
                    ---------------------------------------------------->  
                        C1.start turns red, noPhase+=1
                    
     State trialPhase1   (Next button clicked)                              State trialPhase2
                    --------------------------------------------------------->  
                        C1.start turns black, C1.end turns red, noPhase+=1
                        
     State trialPhase2   (Next button clicked)                              State trialPhase3
                    ------------------------------------------------------->  
                        C2.start turns red, C1.end turns black, noPhase+=1

     State trialPhase3   (Next button clicked)                              State trialPhase4
                    ------------------------------------------------------->  
                        C2.start turns black, C2.end turns red, noPhase+=1
      State trialPhase4   (Next button clicked)                             State trialResult
                    ------------------------------------------------------->     
                            C2.end turns black, noPhase=0
                    
      State trialResult   (Next Trial button clicked) and noTrials+1 < numTrials               State trialStart
                     ---------------------------------------------------------------------->  
                        noPhase=0, noTrials+=1, load noTrials/numTrials, curvature1, curvature2,softness
                     
      State trialResult  (Next Trial button clicked)  and noTrials+1 >= numTrials  and  noBlock+1< numBlocks  and noSeries+1 <numSeries State blockStart
                        ---------------------------------------------------------------------------------------------------------------->
                          noPhase=0,noTrials=0, noBlock+=1,load noBlock/numBlocks, softness in this series, curvature order in this block
                        
     State trialResult  (Next Trial button clicked)  and noTrials+1 >= numTrials  and  noBlock+1>= numBlocks  and  noSeries+1 <numSeries State serieskStart
                      ------------------------------------------------------------------------------------------------------------------>
                         noPhase=0,noTrials=0, noBlock=0,noSeries+=1, load noSeries/numSeries, curvature ref order,softness
    
     State trialResult  (Next Trial button clicked)  and noTrials+1 >= numTrials  and  noBlock+1>= numBlocks  and  noSeries+1 >=numSeries State finished XP
                     ------------------------------------------------------------------------------------------------------------------>
                         noPhase=0,noTrials=0, noBlock=0,noSeries=0
    
"""
class AnotherWindow(QWidget):
    """
    This "window" is a QWidget. If it has no parent,
    it will appear as a free-floating window.
    """

    def __init__(self):
        super().__init__()
        layout = QVBoxLayout()
        self.label = QLabel("Another Window % d" % randint(0, 100))
        layout.addWidget(self.label)
        self.setLayout(layout)
        
class Worker(QRunnable):
    '''
    Worker thread

    Inherits from QRunnable to handler worker thread setup, signals and wrap-up.

    :param callback: The function callback to run on this worker thread. Supplied args and
                     kwargs will be passed through to the runner.
    :type callback: function
    :param args: Arguments to pass to the callback function
    :param kwargs: Keywords to pass to the callback function

    '''

    def __init__(self, fn, *args, **kwargs):
        super(Worker, self).__init__()
        # Store constructor arguments (re-used for processing)
        self.fn = fn
        self.args = args
        self.kwargs = kwargs

    @pyqtSlot()
    def run(self):
        '''
        Initialise the runner function with passed args, kwargs.
        '''
        self.fn(*self.args, **self.kwargs)
        
class Window(QMainWindow):
    """Main window."""
    def __init__(self, parent=None):
        """Initializer."""
        super().__init__(parent)
        
        self.p = None  # Default empty value.
        
        # start page
        self.startPageWin = startPage(self)
        self.startPageWin.show()
        self.startPageWin.ui.startButton.clicked.connect(self.onStartBtnClicked)
        self.numParticipant=12
        self.startPageWin.ui.participantIdSpinBox.setRange(0, self.numParticipant-1)
        self.startPageWin.ui.participantIdSpinBox.valueChanged.connect(self.onparticipantIdSpinBoxValueChanged)
        self.noParticipant=0
        
        
        self.numSeries=3#different ref curvature group
        self.noSeries=0#the serie to begin
        
        self.numBlocks=4#different softness curvature group
        self.noBlock=0#the block to begin
        
        self.numSubBlock=3#different subblocks in each condition
        self.noSubBlock=0#the sub block to begin
        
        self.noTrial=0
        self.numTrials=12
        
        self.noPhase=0
        self.numPhases=4
        
        self.numberTrial=288#the number of trial to begin,used when logging 
        self.result = None
        self.bComMoreCurvedThanRef = 1
        self.dictCompStimulus= None
        
        self.enableSerialPort = True
        
        
    def closeEvent(self):
        if self.enableSerialPort:
            print("Windows was closed")
            
            if self.forceSensorSerialPort.isOpen():
                self.enableReadCompForceSensor=False
                self.enableReadRefForceSensor=False
                time.sleep(0.1)
                self.forceSensorSerialPort.close()
            
        else:
            pass

            
                    
    def readForceSencor(self):
        
        while self.forceSensorSerialPort.isOpen():
            if self.enableReadRefForceSensor or self.enableReadCompForceSensor:
                s = self.forceSensorSerialPort.readline()
                time.sleep(0.01)
                # self.valueForcesensor.clear()
                # self.valueForcesensor.append(str(s))
                # # time.sleep(0.01)
                testStr=str(s.decode("utf-8") )
                testStrToPrint= testStr.replace('\r\n', '')
                print("Force:"+testStrToPrint)
                
                if self.enableSaveRefForceSendorData:
                    self.refForceSensorData.append(testStrToPrint)
                    
                if self.enableSaveCompForceSendorData:
                    self.compForceSensorData.append(testStrToPrint) 
                    
                    
    def getDictCompStimulus(self,refCurvature):
        switcher = {
            10: {'8.3': 1, '8.8': 2, '9.4': 3,'10.7': 4,'11.5': 5,'12.5': 6},
            20: {'16.7': 1, '17.6': 2, '18.8': 3,'21.4': 4,'23.1': 5,'25': 6},
            40: {'32.3': 1, '34.5': 2, '37.0': 3,'43.5': 4,'47.6': 5,'52.6': 6},
        }
 
        return switcher.get(refCurvature, "nothing")
                             
        
    def onparticipantIdSpinBoxValueChanged(self):
        self.noParticipant= self.startPageWin.ui.participantIdSpinBox.value()
        print("noParticipant: ", self.noParticipant)
        
    def onStartBtnClicked(self):
        print("Start button clicked")
        self.experimentParaTree = ElementTree.parse('experimentsPara.xml')
        self.experimentPara = self.experimentParaTree.getroot()
        #self.softnessOrder = self.experimentPara[self.noParticipant].get('softnessOrder')
        self.curvatureRefOrder=self.experimentPara[self.noParticipant].get('refCurvatureOrder')
        
        self.startPageWin.hide()
        self.experimentParaPageWin = experimentParaPage(self)
        self.experimentParaPageWin.show()
        self.experimentParaPageWin.ui.startXpPushButton.clicked.connect(self.onStartXpBtnClicked)
        self.experimentParaPageWin.ui.participantIdTextBrowser.clear()
        self.experimentParaPageWin.ui.participantIdTextBrowser.append(str(self.noParticipant))
        self.experimentParaPageWin.ui.curvatureOrderTextBrowser.append(str(self.curvatureRefOrder))
        
        self.csvDataName= "./data/Participant"+str(self.noParticipant)+"Time"+str(math.floor(time.time()))+".csv"
        Header=['No. Trial','No. Participant','Softness','Ref Curvature','First Curvature','Second Curvature','Which one more curved','Curvature judged more curved','Comp more curved than ref?','First Exploring Time','Second Exploring Time','Switching Time']
        
        with open (self.csvDataName, 'w',encoding='UTF8') as f:
            writer = csv.writer(f)
            writer.writerow(Header)
        # Following code is for choose serial port foe each proto
        ports = list_ports.comports()

        for port, desc, hwid in sorted(ports):
                print(port, desc, hwid)
                # self.experimentParaPageWin.ui.refForceSensorSerialPortcomboBox.addItem(port)
                # self.experimentParaPageWin.ui.compForceSensorSerialPortcomboBox.addItem(port)
                self.experimentParaPageWin.ui.forceSensorSerialPortcomboBox.addItem(port)
                
        if self.enableSerialPort:
            # self.experimentParaPageWin.ui.refForceSensorSerialPortcomboBox.currentIndexChanged.connect(self.refSerialPortComboBoxSelectionChanged)
            # self.experimentParaPageWin.ui.compForceSensorSerialPortcomboBox.currentIndexChanged.connect(self.compSerialPortComboBoxSelectionChanged)
            self.experimentParaPageWin.ui.forceSensorSerialPortcomboBox.currentIndexChanged.connect(self.serialPortComboBoxSelectionChanged)
        else:
            pass
        
        
    def serialPortComboBoxSelectionChanged(self,i):
        self.forceSensorSerialPortName = self.experimentParaPageWin.ui.forceSensorSerialPortcomboBox.currentText()
        print(self.forceSensorSerialPortName)
        
    def onStartXpBtnClicked(self):
        print("Start xp button clicked")

        self.refForceSensorData=[]
        self.enableSaveRefForceSendorData= False
        self.enableReadRefForceSensor = False
        
        self.compForceSensorData=[]
        self.enableSaveCompForceSendorData= False
        self.enableReadCompForceSensor = False
        
        # self.refForceSensorSerialPortName= '/dev/cu.usbmodem143401'
        if self.enableSerialPort:
            self.threadpool = QThreadPool()
            print("Multithreading with maximum %d threads" % self.threadpool.maxThreadCount())
            
                
            self.forceSensorSerialPort=serial.Serial( self.forceSensorSerialPortName)
            if not self.forceSensorSerialPort.isOpen():
                self.forceSensorSerialPort.open()
            else:
                self.forceSensorSerialPort.close()
                self.forceSensorSerialPort.open()
    
            workerReadForceSensor = Worker(self.readForceSencor)
            self.threadpool.start(workerReadForceSensor)
            
            
        else:
            pass
        
        
        
        self.experimentParaPageWin.hide()
        self.seriesStartPageWin = seriesStartPage(self)
        
        self.seriesStartPageWin.ui.startSeriesPushButton.clicked.connect(self.onseriesStartBtnClicked)
        self.seriesStartPageWin.show()
      
        #self.curvatureRefOrder = self.experimentPara[self.noParticipant][self.noSeries].get('curvatureRefOrder')
        self.seriesStartPageWin.ui.noSeriesTextBrowser.clear()
        self.seriesStartPageWin.ui.noSeriesTextBrowser.append(str(self.noSeries+1)+"/"+str(self.numSeries))
        self.seriesStartPageWin.ui.curvatureRefTextBrowser.clear()
        refCurvatures=self.curvatureRefOrder.split(",")
        self.refCurvature=refCurvatures[self.noSeries]
        self.seriesStartPageWin.ui.curvatureRefTextBrowser.append(self.refCurvature)
        self.seriesStartPageWin.ui.softnessOrderTextBrowser.clear()
        #self.softness=self.softnessOrder.split(",")[self.noSeries]
        self.softnessOrder=self.experimentPara[self.noParticipant][self.noSeries].get('softnessOrder')
        self.seriesStartPageWin.ui.softnessOrderTextBrowser.append(self.softnessOrder)
        
        self.seriesStartPageWin.ui.trainingSoftnessTextBrowser.clear()
        self.trainingSoftness=self.experimentPara[self.noParticipant][self.noSeries].get('trainingSoftness')
        self.seriesStartPageWin.ui.trainingSoftnessTextBrowser.append(self.trainingSoftness)
    def onseriesStartBtnClicked(self): 
        print("Start series btn clicked")
        self.seriesStartPageWin.hide()
        self.blockStartPageWin = blockStartPage(self)
        self.blockStartPageWin.show()
        self.blockStartPageWin.ui.startTrialsPushButton.clicked.connect(self.onStartTrialsBtnClicked)
        self.blockStartPageWin.ui.xpBlockNumTextBrowser.clear()
        self.blockStartPageWin.ui.xpBlockNumTextBrowser.append(str(self.noBlock+1)+"/"+ str(self.numBlocks))
        self.blockStartPageWin.ui.xpSubBlockNumTextBrowser.clear()
        self.blockStartPageWin.ui.xpSubBlockNumTextBrowser.append(str(self.noSubBlock+1)+"/"+ str(self.numSubBlock))
        self.blockStartPageWin.ui.softnessTextBrowser.clear()
        self.softness=self.softnessOrder.split(",")[self.noBlock]
        self.blockStartPageWin.ui.softnessTextBrowser.append(self.softness)
        self.curvatureOrder = self.experimentPara[self.noParticipant][self.noSeries][self.noBlock][self.noSubBlock].get('curvatureOrder')
        self.blockStartPageWin.ui.curvatureTextBrowser.clear()
        self.blockStartPageWin.ui.curvatureTextBrowser.append(self.curvatureOrder)
        self.blockStartPageWin.ui.refCurvatureTextBrowser.clear()
        #refCurvatures=self.curvatureRefOrder.split(",")
        self.blockStartPageWin.ui.refCurvatureTextBrowser.append(self.refCurvature)
        self.dictCompStimulus=self.getDictCompStimulus(int(self.refCurvature))
        print("dictionary of comp stimulus: \n")
        print(self.dictCompStimulus)
        #self.refCurvature=refCurvatures[self.noBlock]
    def onStartTrialsBtnClicked(self):
        print("Start trials button clicked")
        self.blockStartPageWin.hide()
        self.trialStartPageWin = trialStartPage(self)
        self.trialStartPageWin.show()
        self.trialStartPageWin.ui.nextPhasePushButton.clicked.connect(self.onNextPhaseBtnClicked)
        self.trialStartPageWin.ui.trialSoftnessTextBrowser.clear()
        self.trialStartPageWin.ui.trialSoftnessTextBrowser.append(self.softness)
        self.firstCurvature=self.experimentPara[self.noParticipant][self.noSeries][self.noBlock][self.noSubBlock][self.noTrial].get('firstCurvature')
        self.secondCurvature=self.experimentPara[self.noParticipant][self.noSeries][self.noBlock][self.noSubBlock][self.noTrial].get('secondCurvature')
        self.trialStartPageWin.ui.trialnNumTextBrowser.clear()
        self.trialStartPageWin.ui.trialnNumTextBrowser.append(str(self.noTrial+1)+"/"+str(self.numTrials))
        self.trialStartPageWin.ui.trialCurvature1TextBrowser.clear()
        self.trialStartPageWin.ui.trialCurvature1TextBrowser.append(self.firstCurvature)
        self.trialStartPageWin.ui.trialCurvature2TextBrowser.clear()
        self.trialStartPageWin.ui.trialCurvature2TextBrowser.append(self.secondCurvature)
        self.trialStartPageWin.ui.noCompStimulusTextBrowser.clear()

    def onNextPhaseBtnClicked(self):
        print("Next trial phase button clicked")
        if self.noPhase==0:
            self.trialStartPageWin.ui.c1StartLabel.setStyleSheet("color:rgb(255,0,0)")
            self.firstCurvatureStartTime=time.time()
            self.trialStartPageWin.ui.trialStartTime1TextBrowser.clear()
            self.trialStartPageWin.ui.trialStartTime1TextBrowser.append(str(self.firstCurvatureStartTime))
            self.trialStartPageWin.ui.noCompStimulusTextBrowser.clear()
            
            if self.enableSerialPort:
                if self.firstCurvature ==self.refCurvature:
                    self.forceSensorSerialPort.write(bytes("7", 'utf-8'))
                    time.sleep(0.1)
                    self.enableReadRefForceSensor =True
                    self.enableSaveRefForceSendorData =True
                else:
                    self.forceSensorSerialPort.write(bytes(str(self.dictCompStimulus[self.firstCurvature]), 'utf-8'))
                    # self.compForceSensorSerialPort.write(bytes("1", 'utf-8'))
                    time.sleep(0.1)
                    self.enableReadCompForceSensor =True
                    self.enableSaveCompForceSendorData =True
                    self.trialStartPageWin.ui.noCompStimulusTextBrowser.clear()
                    self.trialStartPageWin.ui.noCompStimulusTextBrowser.append(str(self.dictCompStimulus[self.firstCurvature]))
            
        elif self.noPhase==1:
            self.trialStartPageWin.ui.c1StartLabel.setStyleSheet("color:rgb(0,0,0)")
            self.trialStartPageWin.ui.c1EndLabel.setStyleSheet("color:rgb(255,0,0)")
            self.firstCurvatureEndTime=time.time()
            self.trialStartPageWin.ui.trialEndTime1TextBrowser.clear()
            self.trialStartPageWin.ui.trialEndTime1TextBrowser.append(str(self.firstCurvatureEndTime))
            if self.enableSerialPort:
                if self.firstCurvature ==self.refCurvature:
                    self.forceSensorSerialPort.write(bytes("0", 'utf-8'))
                    time.sleep(0.1)
                    self.enableReadRefForceSensor =False
                    self.enableSaveRefForceSendorData =False
                else:
                    self.forceSensorSerialPort.write(bytes("0", 'utf-8'))
                    time.sleep(0.1)
                    self.enableReadCompForceSensor =False
                    self.enableSaveCompForceSendorData =False
            self.trialStartPageWin.ui.trialForceTextBrowser.clear()
            # for data in self.refForceSensorData:
            #     self.trialStartPageWin.ui.trialForceTextBrowser.append(data)
            # self.portSerial.hide()
            self.firstExploringTime = float (self.firstCurvatureEndTime)- float (self.firstCurvatureStartTime)
        elif self.noPhase==2:
            self.trialStartPageWin.ui.c1EndLabel.setStyleSheet("color:rgb(0,0,0)")
            self.trialStartPageWin.ui.c2StartLabel.setStyleSheet("color:rgb(255,0,0)")
            self.secondCurvatureStartTime=time.time()
            self.trialStartPageWin.ui.trialStartTime2TextBrowser.clear()
            self.trialStartPageWin.ui.trialStartTime2TextBrowser.append(str(self.secondCurvatureStartTime))
            if self.enableSerialPort:
                if self.secondCurvature ==self.refCurvature:
                    self.forceSensorSerialPort.write(bytes("7", 'utf-8'))
                    time.sleep(0.1)
                    self.enableReadRefForceSensor =True
                    self.enableSaveRefForceSendorData =True
                else:
                    self.forceSensorSerialPort.write(bytes(str(self.dictCompStimulus[self.secondCurvature]), 'utf-8'))
                    # self.compForceSensorSerialPort.write(bytes("1", 'utf-8'))
                    time.sleep(0.1)
                    self.enableReadCompForceSensor =True
                    self.enableSaveCompForceSendorData =True
                    self.trialStartPageWin.ui.noCompStimulusTextBrowser.clear()
                    self.trialStartPageWin.ui.noCompStimulusTextBrowser.append(str(self.dictCompStimulus[self.secondCurvature]))
                
        elif  self.noPhase==3:
            self.trialStartPageWin.ui.c2StartLabel.setStyleSheet("color:rgb(0,0,0)")
            self.trialStartPageWin.ui.c2EndLabel.setStyleSheet("color:rgb(255,0,0)")
            self.secondCurvatureEndTime=time.time()
            self.trialStartPageWin.ui.trialEndTime2TextBrowser.clear()
            self.trialStartPageWin.ui.trialEndTime2TextBrowser.append(str(self.secondCurvatureEndTime))
            if self.enableSerialPort:
                if self.secondCurvature ==self.refCurvature:
                    self.forceSensorSerialPort.write(bytes("0", 'utf-8'))
                    time.sleep(0.1)
                    self.enableReadRefForceSensor =False
                    self.enableSaveRefForceSendorData =False
                else:
                    self.forceSensorSerialPort.write(bytes("0", 'utf-8'))
                    time.sleep(0.1)
                    self.enableReadCompForceSensor =False
                    self.enableSaveCompForceSendorData =False
            self.trialStartPageWin.ui.trialForceTextBrowser.clear()
            # for data in self.refForceSensorData:
            #     self.trialStartPageWin.ui.trialForceTextBrowser.append(data)
            # self.portSerial.hide()
            self.secondExploringTime = float (self.secondCurvatureEndTime)- float (self.secondCurvatureStartTime)
            self.switchingTime = float(self.secondCurvatureStartTime)-float(self.firstCurvatureEndTime)
        else:
            print("this trial finished")        
            self.trialStartPageWin.hide()
            self.trialResultPageWin = trialResultPage(self)
            self.trialResultPageWin.show()
            self.trialResultPageWin.ui.nextTrialPushButton.clicked.connect(self.onNextTrialBtnClicked)
            self.trialStartPageWin.ui.c2EndLabel.setStyleSheet("color:rgb(0,0,0)")
            self.trialResultPageWin.ui.firstRadioButton.toggled.connect(lambda:self.radioBtnState(self.trialResultPageWin.ui.firstRadioButton))
            self.trialResultPageWin.ui.secondRadioButton.toggled.connect(lambda:self.radioBtnState(self.trialResultPageWin.ui.secondRadioButton))
            self.trialStartPageWin.ui.trialStartTime1TextBrowser.clear()
            self.trialStartPageWin.ui.trialEndTime1TextBrowser.clear()
            self.trialStartPageWin.ui.trialStartTime2TextBrowser.clear()
            self.trialStartPageWin.ui.trialEndTime2TextBrowser.clear()
            self.trialStartPageWin.ui.noCompStimulusTextBrowser.clear()
            
        self.noPhase=self.noPhase+1
        
        
    def onNextTrialBtnClicked(self):
        print("Next trial button clicked")
        if self.result ==None:
            pass
        else: 
            if self.noTrial+1<self.numTrials:
                self.writeXpResultToCSV()
                self.noPhase=0
                self.noTrial=self.noTrial+1
                self.trialResultPageWin.hide()
                self.trialStartPageWin.show()
                self.trialStartPageWin.ui.trialSoftnessTextBrowser.clear()
                self.trialStartPageWin.ui.trialSoftnessTextBrowser.append(self.softness)
                self.firstCurvature=self.experimentPara[self.noParticipant][self.noSeries][self.noBlock][self.noSubBlock][self.noTrial].get('firstCurvature')
                self.secondCurvature=self.experimentPara[self.noParticipant][self.noSeries][self.noBlock][self.noSubBlock][self.noTrial].get('secondCurvature')
                self.trialStartPageWin.ui.trialnNumTextBrowser.clear()
                self.trialStartPageWin.ui.trialnNumTextBrowser.append(str(self.noTrial+1)+"/"+str(self.numTrials))
                self.trialStartPageWin.ui.trialCurvature1TextBrowser.clear()
                self.trialStartPageWin.ui.trialCurvature1TextBrowser.append(self.firstCurvature)
                self.trialStartPageWin.ui.trialCurvature2TextBrowser.clear()  
                self.trialStartPageWin.ui.trialCurvature2TextBrowser.append(self.secondCurvature)
                
            elif self.noSubBlock+1<self.numSubBlock:
                self.writeXpResultToCSV()
                print("Series %d block %d subBlock %d finished" %(self.noSeries,self.noBlock,self.noSubBlock))
                self.noTrial=0
                self.noPhase=0
                self.noSubBlock = self.noSubBlock+1
                self.trialResultPageWin.hide()
                self.blockStartPageWin.show()
                
                #self.blockStartPageWin.ui.xpBlockNumTextBrowser.clear()
                #self.blockStartPageWin.ui.xpBlockNumTextBrowser.append(str(self.noBlock+1)+"/"+ str(self.numBlocks))
                self.blockStartPageWin.ui.xpSubBlockNumTextBrowser.clear()
                self.blockStartPageWin.ui.xpSubBlockNumTextBrowser.append(str(self.noSubBlock+1)+"/"+ str(self.numSubBlock))
                self.blockStartPageWin.ui.softnessTextBrowser.clear()
                self.blockStartPageWin.ui.softnessTextBrowser.append(self.softness)
                self.curvatureOrder = self.experimentPara[self.noParticipant][self.noSeries][self.noBlock][self.noSubBlock].get('curvatureOrder')
                self.blockStartPageWin.ui.curvatureTextBrowser.clear()
                self.blockStartPageWin.ui.curvatureTextBrowser.append(self.curvatureOrder)
                refCurvatures=self.curvatureRefOrder.split(",")
                self.blockStartPageWin.ui.refCurvatureTextBrowser.clear()
                self.blockStartPageWin.ui.refCurvatureTextBrowser.append(refCurvatures[self.noSeries])
                self.refCurvature=refCurvatures[self.noSeries]
                
            elif self.noBlock+1 < self.numBlocks:
                self.writeXpResultToCSV()
                print("Series %d block %d finished" %(self.noSeries,self.noBlock))
                self.noTrial=0
                self.noPhase=0
                self.noSubBlock=0
                self.noBlock = self.noBlock+1
                self.trialResultPageWin.hide()
                self.blockStartPageWin.show()
                
                self.blockStartPageWin.ui.xpBlockNumTextBrowser.clear()
                self.blockStartPageWin.ui.xpBlockNumTextBrowser.append(str(self.noBlock+1)+"/"+ str(self.numBlocks))
                self.blockStartPageWin.ui.xpSubBlockNumTextBrowser.clear()
                self.blockStartPageWin.ui.xpSubBlockNumTextBrowser.append(str(self.noSubBlock+1)+"/"+ str(self.numSubBlock))
                self.blockStartPageWin.ui.softnessTextBrowser.clear()
                self.softness=self.softnessOrder.split(",")[self.noBlock]
                self.blockStartPageWin.ui.softnessTextBrowser.append(self.softness)        
                self.curvatureOrder = self.experimentPara[self.noParticipant][self.noSeries][self.noBlock][self.noSubBlock].get('curvatureOrder')
                self.blockStartPageWin.ui.curvatureTextBrowser.clear()
                self.blockStartPageWin.ui.curvatureTextBrowser.append(self.curvatureOrder)
                refCurvatures=self.curvatureRefOrder.split(",")
                self.blockStartPageWin.ui.refCurvatureTextBrowser.clear()
                self.blockStartPageWin.ui.refCurvatureTextBrowser.append(refCurvatures[self.noSeries])
                self.refCurvature=refCurvatures[self.noSeries]
            elif self.noSeries +1 < self.numSeries and self.noBlock+1 >=self.numBlocks:
                 self.writeXpResultToCSV()
                 self.noBlock=0
                 self.noTrial=0
                 self.noPhase=0
                 self.noSubBlock=0
                 print("Series %d finished" %self.noSeries)
                 
                 self.noSeries = self.noSeries+1
                 self.trialResultPageWin.hide()
                 self.seriesStartPageWin.show()
                 
                 #self.curvatureRefOrder = self.experimentPara[self.noParticipant].get('curvatureRefOrder')
                 self.seriesStartPageWin.ui.noSeriesTextBrowser.clear()
                 self.seriesStartPageWin.ui.noSeriesTextBrowser.append(str(self.noSeries+1)+"/"+str(self.numSeries))
                 refCurvatures=self.curvatureRefOrder.split(",")
                 self.refCurvature=refCurvatures[self.noSeries]
                 self.seriesStartPageWin.ui.curvatureRefTextBrowser.clear()
                 self.seriesStartPageWin.ui.curvatureRefTextBrowser.append(self.refCurvature)
                 self.seriesStartPageWin.ui.softnessOrderTextBrowser.clear()
                 self.softnessOrder=self.experimentPara[self.noParticipant][self.noSeries].get('softnessOrder')
                 self.seriesStartPageWin.ui.softnessOrderTextBrowser.append(self.softnessOrder)
                 
            else:
                self.writeXpResultToCSV()
                print("Experiment finished")
                self.finishedPageWin = finishedPage(self)
                self.trialResultPageWin.hide()
                self.finishedPageWin.show()
                
            self.numberTrial=self.numberTrial+1
        
        self.result= None
        self.refForceSensorData=[]
        self.compForceSensorData=[]
        
    def writeXpResultToCSV(self):
        # Header=['No. Trial','No. Participant','Softness','Ref Curvature','First Curvature','Second Curvature','Which one more curved','Curvature judged more curved','Comp more curved than ref?','First Exploring Time','Second Exploring Time','Switching Time']
        dataToWriteTocsv=[self.numberTrial,self.noParticipant,self.softness,self.refCurvature, self.firstCurvature,self.secondCurvature,self.result,self.resultValue,self.bComMoreCurvedThanRef,self.firstExploringTime,self.secondExploringTime,self.switchingTime]
        with open (self.csvDataName, 'a',encoding='UTF8') as f:
            writer = csv.writer(f)
            writer.writerow(dataToWriteTocsv)
            
        
        refForceFileHeader=['No. Trial','No Participant','Softness','Ref Curvature','First Curvature','Second Curvature','Ref Exploring Force']
        refForceCsvDataName="./data/Participant"+str(self.noParticipant)+"Force/"+"Participant"+str(self.noParticipant)+"Trial_No_"+str(self.numberTrial)+"Time"+str(math.floor(time.time()))+"ref.csv"
        dataToWriteToRefForcecsv=[self.numberTrial,self.noParticipant,self.softness,self.refCurvature, self.firstCurvature,self.secondCurvature]
        with open (refForceCsvDataName, 'w',encoding='UTF8') as f:
            writer = csv.writer(f)
            writer.writerow(refForceFileHeader)
            writer.writerow(dataToWriteToRefForcecsv+self.refForceSensorData)
            
            
        compForceFileHeader=['No. Trial','No Participant','Softness','Ref Curvature','First Curvature','Second Curvature','Comp Exploring Force']
        compForceCsvDataName="./data/Participant"+str(self.noParticipant)+"Force/"+"Participant"+str(self.noParticipant)+"Trial_No_"+str(self.numberTrial)+"Time"+str(math.floor(time.time()))+"comp.csv"
        dataToWriteToCompForcecsv=[self.numberTrial,self.noParticipant,self.softness,self.refCurvature, self.firstCurvature,self.secondCurvature]
        with open (compForceCsvDataName, 'w',encoding='UTF8') as f:
            writer = csv.writer(f)
            writer.writerow(compForceFileHeader)
            writer.writerow(dataToWriteToCompForcecsv+self.compForceSensorData)
            
        
    def radioBtnState(self,choice):
        
        if choice.text() == "First":
           if choice.isChecked() == True:
              print (choice.text()+" is selected")
              self.result="first"
              self.resultValue=self.firstCurvature
           # else:
           #    print (choice.text()+" is deselected")
  				
        if choice.text() == "Second":
           if choice.isChecked() == True:
              print(choice.text()+" is selected")
              self.result="second"
              self.resultValue=self.secondCurvature
              
        if self.resultValue == self.refCurvature:
            self.bComMoreCurvedThanRef=0
        else:
            self.bComMoreCurvedThanRef=1
            

class startPage(QtWidgets.QMainWindow):
    """Start page."""
    def __init__(self, parent=None):
        super().__init__(parent)
        # Create an instance of the GUI
        self.ui = Ui_startPage()
        # Run the .setupUi() method to show the GUI
        self.ui.setupUi(self)

class experimentParaPage(QtWidgets.QMainWindow):
    """Start page."""
    def __init__(self, parent=None):
        super().__init__(parent)
        # Create an instance of the GUI
        self.ui = Ui_experimentParaPage()
        # Run the .setupUi() method to show the GUI
        self.ui.setupUi(self)
        
        
class seriesStartPage(QtWidgets.QMainWindow):
    """Series start page"""
    def __init__(self, parent=None):
        super().__init__(parent)
        # Create an instance of the GUI
        self.ui = Ui_seriesStartPage()
        # Run the .setupUi() method to show the GUI
        self.ui.setupUi(self)
        
        
class blockStartPage(QtWidgets.QMainWindow):
    """Block start page"""
    def __init__(self, parent=None):
        super().__init__(parent)
        # Create an instance of the GUI
        self.ui = Ui_blockStartPage()
        # Run the .setupUi() method to show the GUI
        self.ui.setupUi(self)
  
class trialStartPage(QtWidgets.QMainWindow):
    """Trial start page"""
    def __init__(self, parent=None):
        super().__init__(parent)
        # Create an instance of the GUI
        self.ui = Ui_trialStartPage()
        # Run the .setupUi() method to show the GUI
        self.ui.setupUi(self)        

class trialResultPage(QtWidgets.QMainWindow):
    """Trial result page"""
    def __init__(self, parent=None):
        super().__init__(parent)
        # Create an instance of the GUI
        self.ui = Ui_trialResultPage()
        # Run the .setupUi() method to show the GUI
        self.ui.setupUi(self)  
        
class finishedPage(QtWidgets.QMainWindow):
    """Finished page"""
    def __init__(self, parent=None):
        super().__init__(parent)
        # Create an instance of the GUI
        self.ui = Ui_finishedPage()
        # Run the .setupUi() method to show the GUI
        self.ui.setupUi(self)     
        

class WorkerSignals(QObject):
    '''
    Defines the signals available from a running worker thread.

    Supported signals are:

    finished
        No data

    error
        tuple (exctype, value, traceback.format_exc() )

    result
        object data returned from processing, anything

    progress
        int indicating % progress

    '''
    finished = pyqtSignal()
    error = pyqtSignal(tuple)
    result = pyqtSignal(object)
    progress = pyqtSignal(int)
    

if __name__ == "__main__":
    # Create the application
    app = QApplication(sys.argv)
    
    
    # Create and show the application's main window
    win = Window()
    # win.show()
    app.lastWindowClosed.connect(win.closeEvent)
    # Run the application's main loop
    sys.exit(app.exec())
