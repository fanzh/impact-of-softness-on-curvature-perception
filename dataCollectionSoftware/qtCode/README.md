## Folder `qtCode`
This folder contains codes for our major data-collecting software developed in Python with PyQT5. 

- File `createXpParaXML.py` An Python script used to create the configuration file for the experiment, i.e., the order of presenting stimuli for each participant.

-File `experimentsParaFixed.xml` the experiment configuration file that we used for our experiment.

- File `softnessCurvature.py` The major Python script works as our data-collecting software

- Qt-UI files,e.g., `experimentParaPage.ui` are GUIs designed with Qt Designer. Respective Python files, e.g., `experimentParaPage.py` are python files automatically created with Qt Designer. These python files will be called by the script `softnessCurvature.py` during the experiment.

- Folder `data` 
Major experiment data of each participant being collected will be saved in this folder as CSV files. Subsidiary folder, e.g., Participant10Force will save each participant's exploring force data.
