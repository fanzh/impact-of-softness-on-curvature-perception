#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec  3 10:08:47 2021

@author: fanzhuzhi
"""

from xml.etree import ElementTree, cElementTree
from xml.etree.ElementTree import Element,SubElement,Comment,tostring
import sys
import random
from xml.dom import minidom

'''
XP parameters structure:

data[numParticipants='']
    participant[noParticipant='',numSeries='',softnessOrder='']
        series[noSeries='',numBlocks='',softness='',curvatureRefOrder='']
            block[noBlock='',numTrials='',refCurvature='',curvatureOrder='']
                trial[noTrial='',firstCurvature='',secondCurvature='']

Balanced Latin Square
"00-10","00-50","Rigid","A-30" 0 1 3 2
"00-50","A-30","00-10","Rigid" 1 2 0 3
"A-30","Rigid","00-50","00-10" 2 3 1 0
"Rigid","00-10","A-30","00-50" 3 0 2 1

'''
def createSoftnessOrder(noParticipant):
    noSoftnessOrder=noParticipant%4
    listAllSoftnessOrder={
            0:"00-10,00-50,Rigid,A-30",
            1:"00-50,A-30,00-10,Rigid",
            2:"A-30,Rigid,00-50,00-10",
            3:"Rigid,00-10,A-30,00-50,",
        }
    return listAllSoftnessOrder.get(noSoftnessOrder)

def createRefCurvatureOrder(noParticipant):
    noRefCurvatureOrder=noParticipant%4
    listAllRefCurvatureOrder={
            0:"10,20,40",
            1:"10,40,20",
            2:"40,10,20",
            3:"40,20,10",
            4:"20,40,10",
            5:"20,10,40",
        }
    return listAllRefCurvatureOrder.get(noRefCurvatureOrder)




class prototypeCurvature(object):
   def __init__(self,curvatureValue=0,presentedOrder=1):
        self.curvatureValue=curvatureValue
        self.presentedOrder=presentedOrder


def createCurvatureOrder(refCurvature):
    listCurvatureOrder={
            '10':random.sample([prototypeCurvature(8.3,1),prototypeCurvature(8.8,1),prototypeCurvature(9.4,1),prototypeCurvature(10.7,1),
                              prototypeCurvature(11.5,1),prototypeCurvature(12.5,1),prototypeCurvature(8.3,2),prototypeCurvature(8.8,2),
                              prototypeCurvature(9.4,2),prototypeCurvature(10.7,2),prototypeCurvature(11.5,2),prototypeCurvature(12.5,2)],k=numTrials),
            '20':random.sample([prototypeCurvature(16.7,1),prototypeCurvature(17.6,1),prototypeCurvature(18.8,1),prototypeCurvature(21.4,1),
                              prototypeCurvature(23.1,1),prototypeCurvature(25,1),prototypeCurvature(16.7,2),prototypeCurvature(17.6,2),
                              prototypeCurvature(18.8,2),prototypeCurvature(21.4,2),prototypeCurvature(23.1,2),prototypeCurvature(25,2)],k=numTrials),
            '40':random.sample([prototypeCurvature(32.3,1),prototypeCurvature(34.5,1),prototypeCurvature(37.0,1),prototypeCurvature(43.5,1),
                              prototypeCurvature(47.6,1),prototypeCurvature(52.6,1),prototypeCurvature(32.3,2),prototypeCurvature(34.5,2),
                              prototypeCurvature(37.0,2),prototypeCurvature(43.5,2),prototypeCurvature(47.6,2),prototypeCurvature(52.6,2)],k=numTrials)
        }
    
    curvatureOrderList=listCurvatureOrder.get(refCurvature)
    curvatureOrder=""
    for protoCurvature in curvatureOrderList:
        if protoCurvature.presentedOrder == 1:
            tempCurvatureValue=str(protoCurvature.curvatureValue)+","+str(refCurvature)
        else:
            tempCurvatureValue=str(refCurvature)+","+str(protoCurvature.curvatureValue)
        
        curvatureOrder=curvatureOrder+tempCurvatureValue+";"
            
    return curvatureOrder[0:-1]
    
    
softnessOrder=["00-10","00-50","A-30","Rigid"]
curvatureValue=[[8.3,8.8,9.4,10.7,11.5,12.5],
                [16.7,17.6,18.8,21.4,23.1,25],
                [32.3,34.5,37.0,43.5,47.6,52.6]]
# print(curvatureValue)

numParticipants=12
numSeries=3
numBlocks=4
numSubBlocks=3
numTrials=12

data= Element('data')
data.set('numParticipants', str(numParticipants))


for noParticipant in range(numParticipants):
    print("noParticipant: ", noParticipant)
    participant=SubElement(data,'participant')
    participant.set('noParticipant',str(noParticipant))
    participant.set('numSeries',str(numSeries))
    listCurvatureRefOrder=random.sample([10,20,40],k=3)
    participant.set('refCurvatureOrder',str(listCurvatureRefOrder[0])+","+str(listCurvatureRefOrder[1])+","+str(listCurvatureRefOrder[2]))
    #participant.set('refCurvatureOrder',createRefCurvatureOrder(noParticipant))
    participant.set('softnessOrder',createSoftnessOrder(noParticipant))
    listSoftnessOrder=participant.get('softnessOrder').split(",")
    listCurvatureRefOrder=participant.get('refCurvatureOrder').split(",")
    print('Ref Curvature Order:',participant.get('refCurvatureOrder'))
    for noSeries in range(numSeries):
        print("\t noSeries: ",noSeries)
        # print("softness:", listSoftnessOrder[noSeries])
        series=SubElement(participant,'series')
        series.set('noSeries', str(noSeries))
        series.set('numBlocks', str(numBlocks))
        series.set('softnessOrder',str(listSoftnessOrder[0])+","+str(listSoftnessOrder[1])+","+str(listSoftnessOrder[2])+","+str(listSoftnessOrder[3]))
        series.set('trainingSoftness', str(random.sample(["00-10","00-50","A-30","Rigid"],k=1)[0]))
        #series.set('softness', listSoftnessOrder[noSeries])
        #listCurvatureRefOrder=random.sample([10,20,40],k=3)
        series.set('refCurvature',listCurvatureRefOrder[noSeries])
        for noBlock in range(numBlocks):
            print("\t \t noBlock: ",noBlock)
            block=SubElement(series,'block')
            block.set('noBlock',str(noBlock))
            block.set('numTrials',str(numTrials))
            block.set('refCurvature',listCurvatureRefOrder[noSeries])
            block.set('softness',str(listSoftnessOrder[noBlock]))
            #block.set('curvatureOrder',createCurvatureOrder(listCurvatureRefOrder[noSeries]))
            #curvatureOrder=block.get('curvatureOrder').split(";")
            for noSubBlock in range(numSubBlocks):
                print("\t \t noSubBlock: ",noSubBlock)
                subBlock=SubElement(block,'subBlock')
                subBlock.set('noSubBlock',str(noSubBlock))
                subBlock.set('numTrials',str(numTrials))
                subBlock.set('refCurvature',str(listCurvatureRefOrder[noSeries]))
                subBlock.set('curvatureOrder',createCurvatureOrder(listCurvatureRefOrder[noSeries]))
                curvatureOrder=subBlock.get('curvatureOrder').split(";")
                for noTrial in range(numTrials):
                    print("\t \t \t noTrial: ",noTrial)
                    trial=SubElement(subBlock,'trial')
                    trial.set('noTrial',str(noTrial))
                    trial.set('firstCurvature',curvatureOrder[noTrial].split(",")[0])
                    trial.set('secondCurvature',curvatureOrder[noTrial].split(",")[1])

experimentsPara=cElementTree.ElementTree(data)

experimentsPara.write('experimentsPara.xml')


# tree = ElementTree.parse('ExperimentsPara.xml')
# root =tree.getroot()
# # xpParadata='ExperimentsPara.xml'
# # root = ElementTree.fromstring(xpParadata)

# # print (root.tag)

# # for child in root:
# #     print("tag: ", child.tag,"attrib: ", child.attrib)
# #     for suchild in child:
# #         print("sub_tag: ", suchild.tag,"sub_attrib: ", suchild.attrib)
        
# # print(root[0][0][0][0].attrib)

# # xpParaDic=root[0][0][0][0].attrib
# # print(xpParaDic['RepeatNum'])
# # print(xpParaDic.get('Direction'))

# # print("Tag:",root[0][0][0][0].tag)
# # print("text:",root[0][0][0][0].text)
# # print("tail:",root[0][0][0][0].tail)

# print(root.findall("./Experimentation[@id='0']/Training/TA/Para"))


