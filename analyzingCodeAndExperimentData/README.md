## Folder `analyzingCodeAndExperimentData`
This folder contains one folder `data` where you can find our experiment data, our measurement data of softness and curvature of our stimuli and the R codes to analyze the data, and the results from our experiment presented in the paper https://doi.org/10.1145/3544548.3581179 .

- File `ImpactSoftnessOnCurvaturePerception.Rproj`
An R project file that can be opened with the Rstudio software to run the analysis code

- File `Planned-analyzes Impact of Softness on Curvature Perception.Rmd`
An R Markdown file that includes our main code to analyze the data collected during our experiment.

- File `Significance test on Impact of Softness on Curvature Perception.Rmd`
An R Markdown file that includes our main code to do the statistical test on the data collected during our experiment.

- File `references.bib`
References used in `Planned-analyzes Impact of Softness on Curvature Perception.Rmd`

- File `acm-sigchi-proceedings.csl`
Reference format file used in `Planned-analyzes Impact of Softness on Curvature Perception.Rmd`

- File `Difficulty Estimation.Rmd`
An R Markdown file containing our code to analyze the questionnaire data on participants’ curvature perception difficulty estimation.

- File `Significant test of Difficulty Estimation.Rmd`
An R Markdown file containing our code to do the statistical test questionnaire data on participants’ curvature perception difficulty estimation.

- File `curvature calculation.Rmd`
An R Markdown file containing our code to visualize and analyze our data of curvature measurement.

- File `softnessMeasurement.Rmd`
An R Markdown file containing our code to visualize and analyze our data of softness measurement.

- File `calculate curvature.py`
A Python script file containing our code to calculate the curvature of our stimuli.

### Folder `data`
This folder contains four folders: `experimentData`, `questionnaire`, `curvatureData`, `softnessData`, and `forceData`. Below is the description of each folder.

- Folder `experimentData`
This folder contains the data we collected during our experiment in CSV format. These files contain participants’ original responses of which stimulus is more curved in each trial and corresponding trial factors. Factors include the number of trials, the softness condition, the curvature condition, the curvature of the stimulus presented first, and the curvature of the stimulus presented second. You can also find the exploration time for each stimulus in each trial.

- Folder `questionnaire`
This folder contains a CSV file that records the subjective difficulty reported by participants for each condition.

- Folder `curvatureData`
This folder contains a csv file `fittingRsult.csv` that contains our measurement result of the curvature of our stimuli

- Folder `softnessData`
This folder contains csv files recording our measurement result of the softness of our stimuli, a folder `shoreHardnessMeasurement` containing the shore hardness measurement result of our stimuli, and a folder `previousWork` containing data reproduced from the previous work [10.1109/TOH.2015.2391093].


- Folder `forceData`
This folder contains csv files recording our measurement result of participants' exploring force during the experiment. Further updates of this git repository will add code for analyzing participants' exploring force data.
