#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 17 17:51:40 2022

@author: "Zhuzhi Fan"
"""

import cv2 
import sys
import numpy as np
from circle_fit import hyper_fit, least_squares_circle, plot_data_circle
from matplotlib import pyplot as plt, cm, colors
#from math import pi
import os
import re
import csv
import glob
import pandas as pd

from RANSAC.Common import Util
from RANSAC.Common import CircleModel
from RANSAC.Common import Point
from RANSAC.Algorithm import RansacCircleHelper
import traceback
from RANSAC.RANSAC.MatplotUtil import plot_new_points_over_existing_points



mmPerPixel=101.32/1930

class myCircleModel(CircleModel):
    def __init__(self, center_x:float,center_y:float,radius:float):
        CircleModel.__init__(self, center_x,center_y,radius)
        
    @classmethod
    def getCircleInfo(cls,model,distance=1):
        xcenter=model.X
        ycenter=model.Y
        radius=model.R
        
        return (xcenter,ycenter,radius)
        
    
class circleInformation:
    def __init__(self,rInPixel=0,Rsquared=0,rDesigned=0,softness="",noFigure=0):
        self.rInPixel=rInPixel
        self.rInMM=self.rInPixel*mmPerPixel
        self.Rsquared=Rsquared
        self.rDesigned=rDesigned
        self.diffR=self.rInMM-self.rDesigned
        self.softness=softness
        self.noFigure=noFigure
        
        
        
def calculateRsquared(x,y,xc,yc,R):
    y_fit=yc+np.sqrt(R**2-abs(xc-x)**2)
    # residual sum of squares
    ss_res = np.sum((y - y_fit) ** 2)
    
    # total sum of squares
    ss_tot = np.sum((y - np.mean(y)) ** 2)
    
    # r-squared
    r2 = 1 - (ss_res / ss_tot)
    
    return r2

def plot_fitted_circle(x, y, xc, yc, R,circleInfoThisFigure):
    """
    Plot data and a fitted circle.
    Inputs:

        x : data, x values (array)
        y : data, y values (array)
        xc : fit circle center (x-value) (float)
        yc : fit circle center (y-value) (float)
        R : fir circle radius (float)

    Output:
        None (generates matplotlib plot).
    """
    f = plt.figure(facecolor='white')
    plt.axis('equal')

    
    x=x-xc
    y=y-yc
    x=x*mmPerPixel
    y=y*mmPerPixel
    R=R*mmPerPixel
    xmin=np.min(x)
    xmax=np.max(x)
    
    thetaMin=np.arccos(xmin/R)
    thetaMax=np.arccos(xmax/R)
    theta_fit = np.linspace(thetaMin, thetaMax, 1800)
    
    x_fit = R*np.cos(theta_fit)
    y_fit = R*np.sin(theta_fit)
    
    #plt.plot(0, 0, 'bD', mec='y', mew=1)
    plt.xlabel('x')
    plt.ylabel('y')
    # plot data
    plt.scatter(x, y, c='red', label='data',s=1)

    plt.plot(x_fit, y_fit, 'b-' , label="fitted circle", lw=0.5)
    
    plt.legend(loc='best',labelspacing=0.1 )
    plt.grid()
    plt.title('Fit Circle')
    plt.savefig(path+intermediateResultPath+"_".join([circleInfoThisFigure.softness,
                                                      str(circleInfoThisFigure.rDesigned),
                                                      str(circleInfoThisFigure.noFigure)])+"fittedCircle.png")
    
#To perform the analysis yourself, download the raw data logs from figure folder    
path="./figure/"
dir_list = os.listdir(path+"DONE/")
#dir_list = os.listdir(path)

intermediateResultPath="intermediate/"

try: 
    os.mkdir(path+intermediateResultPath) 
except OSError as error: 
    print(error) 
    
figureWithEdgeDetectedPath="figureWithEdgeDetected/"
try: 
    os.mkdir(path+figureWithEdgeDetectedPath) 
except OSError as error: 
    print(error) 
    
figureWithEdgeFittedPath="figureWithEdgeFitted/"
try: 
    os.mkdir(path+figureWithEdgeFittedPath) 
except OSError as error: 
    print(error) 
    
     
fittingResultPath="result/"
try: 
    os.mkdir(path+fittingResultPath) 
except OSError as error: 
    print(error) 

done_list = os.listdir(path+fittingResultPath)

def writeFittingResultToCsv(fittingResult):
    csvDataName=[fittingResult.softness,str(fittingResult.rDesigned),str(fittingResult.noFigure)]
    csvDataName="_".join(csvDataName)
    csvDataName=csvDataName+".csv"
    csvDataName=path+fittingResultPath+csvDataName
    Header=['softness','rDesigned','rInMM','diffR','Rsquared','noFigure','rInPixel']
    dataToWriteTocsv=[fittingResult.softness,
                      fittingResult.rDesigned,
                      fittingResult.rInMM,
                      fittingResult.diffR,
                      fittingResult.Rsquared,
                      fittingResult.noFigure,
                      fittingResult.rInPixel]
    with open (csvDataName, 'w',encoding='UTF8') as f:
        writer = csv.writer(f)
        writer.writerow(Header)
        writer.writerow(dataToWriteTocsv)
    


def calculteSobelEdge(sobelXY):
    
    sobelxabs=np.absolute(sobelXY)
    sobelxabs[sobelxabs<10]=0
    # cv2.imshow('sobelxabs', sobelxabs)
    edgeSobel=cv2.GaussianBlur(sobelxabs, (3,3), 0)
    return edgeSobel
   
dir_list=['00-10_21.4_7.tiff']      
for file in dir_list:
    if file.endswith(".tiff"):
        print(file)
        img = cv2.imread(path+"DONE/"+file)
        #cv2.imshow(file,img)
        
        fileNameInfo=re.findall("^(.*)\_(.*)\_(.*).tiff$", file)
        softness=fileNameInfo[0][0]
        rDesigned=float(fileNameInfo[0][1])
        noFigure=fileNameInfo[0][2]
        
        resultName=[softness,str(rDesigned),str(noFigure)]
        resultName="_".join(resultName)
        resultName=resultName+".csv"
        # if resultName in done_list:
        #     print("done")
        #     continue
        
        if img is None:
            sys.exit("Could not read the image.")
        #cv2.imshow("Display window", img)
        img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        #cv2.imshow('img_gray', img_gray)
        img_blur = cv2.GaussianBlur(img_gray, (11,11), 0) 
        #cv2.imshow('img_blur', img_blur)
        	
        # Canny Edge Detection
        edges = cv2.Canny(image=img_blur, threshold1=100, threshold2=200) # Canny Edge Detection
        # Display Canny Edge Image
        #cv2.imshow('Canny Edge Detection', edges)
        
        #here is the algorithm hyper_fit
        edgePosition = np.where(edges!=0)
        edgePositionX=edgePosition[1]
        edgePositionY=edgePosition[0]*-1
        positionXY=np.stack((edgePositionX, edgePositionY), axis=1)
        #circle=least_squares_circle(positionXY)
        circle=hyper_fit(positionXY,verbose=False)
        xCenter=circle[0]
        yCenter=circle[1]
        rCircle=circle[2]
        rCircleInMM=rCircle*mmPerPixel
        
        
        Rsquared=calculateRsquared(edgePositionX,edgePositionY,xCenter,yCenter,rCircle)
        
        circleInfoThisFigure= circleInformation(rDesigned=rDesigned,
                                                softness=softness,
                                                noFigure=noFigure,
                                                rInPixel=rCircle,
                                                Rsquared=Rsquared)
        
        writeFittingResultToCsv(circleInfoThisFigure)
        
        # plot_fitted_circle(edgePositionX,edgePositionY,xCenter,yCenter,rCircle,circleInfoThisFigure)
        
        imagWithEdge=img.copy()
        imagWithEdge[edgePosition]=[0,0,255]
        
        contours, _ = cv2.findContours(edges, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        # draw the contours on a copy of the original image
        cv2.drawContours(imagWithEdge, contours, -1, (0, 0, 255), 3)
    
        #cv2.imwrite(path+figureWithEdgeDetectedPath+"_".join([softness,str(rDesigned),str(noFigure)])+".png", imagWithEdge)
        
        #cv2.imwrite(path+intermediateResultPath+"_".join([softness,str(rDesigned),str(noFigure)])+"edges.png", edges)
        



result_list = glob.glob(path+fittingResultPath+'*.{}'.format('csv'))

df_append = pd.concat([pd.read_csv(f) for f in result_list ], ignore_index=True)
            
df_append.to_csv("./fittingRsult.csv")


